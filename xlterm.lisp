(in-package #:xlterm)

(cffi:defcfun ("execve" execve) :int
  (filename :string)
  (argv (:pointer :string))
  (envp (:pointer :string)))

(defun find-user-config ()
  (let ((user-config (uiop:merge-pathnames* "xltermrc" (uiop:xdg-config-home))))
    (when (probe-file user-config)
      user-config)))

(defun main ()
  (let ((application (make-instance 'application)))
    (unwind-protect
         (handler-case
             (let ((user-config (find-user-config))
                   (*application* application))
               (when user-config
                 (load user-config))
               (start *application*))
           (sb-sys:interactive-interrupt ()
             (sb-ext:quit)))
      (stop application))))

(defun start-shell ()
  (cffi:with-foreign-object (amaster :int)
    (let ((pid (forkpty amaster (cffi:null-pointer) (cffi:null-pointer) (cffi:null-pointer))))
      (if (= pid 0)
          (cffi:with-foreign-objects ((argv :string 2)
                                      (envp :string 2))
            (setf (cffi:mem-aref argv :string) "/bin/bash")
            (setf (cffi:mem-aref argv :string 1) (cffi:null-pointer))

            (setf (cffi:mem-aref envp :string) "TERM=xterm")
            (setf (cffi:mem-aref envp :string 1) (cffi:null-pointer))

            (execve "/bin/bash" argv envp))
          (values pid (cffi:mem-ref amaster :int))))))
