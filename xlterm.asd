(defsystem #:xlterm
  :name "xlterm"
  :author "Florian Margaine <florian@margaine.com>"
  :description "XL Term"
  :defsystem-depends-on ("cffi-grovel")
  :depends-on ("cffi" "clx" "bordeaux-threads" "flexi-streams")
  :components ((:file "package")
               (:cffi-grovel-file "grovel-pty" :depends-on ("package"))
               (:cffi-wrapper-file "grovel-pty-wrapper" :depends-on ("grovel-pty" "package"))
               (:file "xlterm" :depends-on ("package" "grovel-pty" "application"))
               (:file "window" :depends-on ("package"))
               (:file "application" :depends-on ("package" "window" "parser"))
               (:file "parser" :depends-on ("package" "window"))))
