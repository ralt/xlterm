(in-package #:xlterm)

(defclass parser-base-class ()
  ((window :initarg :window :reader window)
   (default-foreground :initarg :default-foreground :reader default-foreground)
   (input :initarg :input :reader input)
   (text :initarg :text :accessor text :initform "")
   (index :initarg :index :reader index :initform 0)
   (contexts :initarg :contexts :accessor contexts)
   (arguments :accessor arguments :initform nil)))

(defvar *escape-square-bracket-terminator*
  (list (char-code #\m)))

(defvar *terminator-classes* (make-hash-table))

(defmacro define-terminator-parser (terminator (var) &body body)
  (let ((terminator-class-symbol
         (intern (string-upcase (format nil "terminator-parser-~a" terminator)))))
    `(progn
       (defclass ,terminator-class-symbol (parser-base-class) ())

       (setf (gethash ,terminator *terminator-classes*) ',terminator-class-symbol)

       (defmethod initialize-instance :after ((,var ,terminator-class-symbol) &key)
                  ,@body))))

(defvar *colormap* (make-hash-table))
(setf (gethash 30 *colormap*) "black")
(setf (gethash 31 *colormap*) "red")
(setf (gethash 32 *colormap*) "green")
(setf (gethash 33 *colormap*) "yellow")
(setf (gethash 34 *colormap*) "blue")
(setf (gethash 35 *colormap*) "magenta")
(setf (gethash 36 *colormap*) "cyan")
(setf (gethash 37 *colormap*) "white")

(define-terminator-parser #\m (parser)
  (add (contexts parser)
       (make-instance 'xlib-context
                      :text-color (if (arguments parser)
                                      (gethash (parse-integer
                                                (format nil "~{~a~}"
                                                        (mapcar
                                                         #'code-char
                                                         (arguments parser))))
                                               *colormap*)
                                      (default-foreground parser))
                      :text ""
                      :x (text-width (window parser)
                                     (text parser))
                      :y (font-ascent (window parser))))
  (parse parser))

(defclass parser-escape-square-brackets (parser-base-class) ())

(defmethod initialize-instance :after ((parser parser-escape-square-brackets) &key)
  (let ((elt))
    (loop for i from (index parser) below (- (length (input parser)) (index parser))
       do (setf elt (elt (input parser) i))
       when (member elt *escape-square-bracket-terminator*)
       do (return-from initialize-instance
            (make-instance (gethash (code-char elt) *terminator-classes*)
                           :window (window parser)
                           :default-foreground (default-foreground parser)
                           :text (text parser)
                           :contexts (contexts parser)
                           :input (input parser)
                           :index (1+ (+ (index parser) i))))
       do (appendf (arguments parser) (list elt)))))

(defclass parser-escape (parser-base-class) ())

(defmethod initialize-instance :after ((parser parser-escape) &key)
  (when (= (elt (input parser) (index parser)) 93)
    (return-from initialize-instance
      (make-instance 'parser-escape-square-brackets
                     :window (window parser)
                     :default-foreground (default-foreground parser)
                     :text (text parser)
                     :contexts (contexts parser)
                     :input (input parser)
                     :index (1+ (index parser))))))

(defclass parser (parser-base-class) ())

(defmethod initialize-instance :after ((parser parser) &key)
  (setf (contexts parser) (make-instance 'xlib-contexts))
  (when (= (length (input parser)) 0)
    (return-from initialize-instance))
  (add (contexts parser)
       (make-instance 'xlib-context
                      :text-color (default-foreground parser)
                      :text ""
                      :x (text-width (window parser)
                                     (text parser))
                      :y (font-ascent (window parser))))
  (parse parser))

(defmethod parse ((parser parser-base-class))
  (let ((elt)
        (last-context (last-context (contexts parser))))
    (loop for i from (index parser) below (length (input parser))
       do (setf elt (elt (input parser) i))
       when (= elt 27)
       do (return-from parse
            (make-instance 'parser-escape
                           :window (window parser)
                           :default-foreground (default-foreground parser)
                           :text (text parser)
                           :contexts (contexts parser)
                           :input (input parser)
                           :index (1+ (+ (index parser) i))))
       do (setf (text parser) (format nil "~a~a"
                                      (text parser)
                                      (code-char elt)))
       do (setf (text last-context)
                (format nil "~a~a"
                        (text last-context)
                        (code-char elt))))))
