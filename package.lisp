(defpackage #:xlterm
  (:use :common-lisp))

(in-package #:xlterm)

(define-modify-macro appendf (&rest args)
  append "Append onto list")
