(in-package #:xlterm)

(include "pty.h")

(ctype pid-t "pid_t")

(cstruct winsize "struct winsize"
  (ws-row "ws_row" :type :unsigned-short)
  (ws-col "ws_col" :type :unsigned-short)
  (ws-xpixel "ws_xpixel" :type :unsigned-short)
  (ws-ypixel "ws_ypixel" :type :unsigned-short))

(ctype tcflag-t "tcflag_t")
(ctype cc-t "cc_t")

(cstruct termios "struct termios"
  (c-iflag "c_iflag" :type tcflag-t)
  (c-oflag "c_oflag" :type tcflag-t)
  (c-cflag "c_cflag" :type tcflag-t)
  (c-lflag "c_lflag" :type tcflag-t)
  (c-line "c_line" :type cc-t)
  (c-cc "c_cc" :type cc-t :count 19))
