(in-package #:xlterm)

(defclass window ()
  ((background-color :initarg :background-color :reader background-color)
   (foreground-color :initarg :foreground-color :reader foreground-color)
   (font-family :initarg :font-family :reader font-family)
   (title :initarg :title :reader title)
   (event-mask :initarg :event-mask :reader event-mask)
   (display :accessor display)
   (screen :accessor screen)
   (colormap :accessor colormap)
   (font :accessor font)
   (xlib-window :accessor xlib-window)
   (background :accessor background)
   (gcontext :accessor gcontext)))

(defmethod initialize-instance :after ((window window) &key)
  (setf (display window) (xlib:open-default-display))
  (setf (screen window) (xlib:display-default-screen (display window)))
  (setf (colormap window) (xlib:screen-default-colormap (screen window)))
  (setf (font window) (xlib:open-font (display window) (font-family window)))
  (setf (xlib-window window)
        (multiple-value-bind (width ascent)
            (xlib:text-extents (font window) "A")
          (xlib:create-window
           :parent (xlib:screen-root (screen window))
           :x 512
           :y 512
           :width (+ 80 width)
           :height (+ 20 ascent)
           :background (xlib:alloc-color (colormap window)
                                         (xlib:lookup-color (colormap window)
                                                            (background-color window)))
           :event-mask (apply #'xlib:make-event-mask (event-mask window)))))
  (setf (background window)
        (xlib:create-gcontext
         :drawable (xlib-window window)
         :fill-style :solid
         :background (xlib:screen-white-pixel (screen window))
         :foreground (xlib:alloc-color (colormap window)
                                       (xlib:lookup-color (colormap window)
                                                          (background-color window)))
         :font (font window)))
  (setf (gcontext window)
        (xlib:create-gcontext
         :drawable (xlib-window window)
         :fill-style :solid
         :background (xlib:screen-white-pixel (screen window))
         :foreground (xlib:alloc-color (colormap window)
                                       (xlib:lookup-color
                                        (colormap window)
                                        (foreground-color window)))
         :font (font window)))

  (xlib:set-wm-properties (xlib-window window) :name (title window)))

(defmethod draw ((window window))
  (xlib:map-window (xlib-window window))
  (xlib:draw-rectangle (xlib-window window) (background window)
                       0 0
                       (xlib:drawable-width (xlib-window window))
                       (xlib:drawable-height (xlib-window window))
                       :fill-p))

(defmethod destroy ((window window))
  (xlib:destroy-window (xlib-window window))
  (xlib:close-display (xlib:window-display (xlib-window window))))

(defmethod draw-line ((window window) x y line)
  (xlib:draw-glyphs (xlib-window window) (gcontext window)
                    x y
                    line))

(defmethod font-ascent ((window window))
  (xlib:font-ascent (font window)))

(defmethod flush ((window window))
  (xlib:display-force-output (display window)))

(defmacro on-event (window &body events)
  `(xlib:event-case ((display ,window))
     ,@events))

(defmethod code->char ((window window) code)
  (xlib:keycode->character (display window) code 0))

(defmethod clear ((window window))
  (xlib:clear-area (xlib-window window)))

(defmethod height ((window window))
  (xlib:drawable-height (xlib-window window)))

(defmethod draw-text ((window window) contexts)
  (iter contexts
        (lambda (context)
          (xlib:draw-glyphs (xlib-window window)
                            (xlib:create-gcontext
                             :drawable (xlib-window window)
                             :fill-style :solid
                             :background (xlib:screen-white-pixel (screen window))
                             :foreground (xlib:alloc-color (colormap window)
                                                           (xlib:lookup-color
                                                            (colormap window)
                                                            (text-color context)))
                             :font (font window))
                            (x context)
                            (y context)
                            (text context)))))

(defclass xlib-context ()
  ((text-color :initarg :text-color :reader text-color)
   (text :initarg :text :accessor text)
   (x :initarg :x :reader x)
   (y :initarg :y :reader y)))

(defclass xlib-contexts ()
  ((contexts :initform nil)))

(defmethod add ((contexts xlib-contexts) context)
  (appendf (slot-value contexts 'contexts) (list context)))

(defmethod iter ((contexts xlib-contexts) fn)
  (loop for context in (slot-value contexts 'contexts) do (funcall fn context)))

(defmethod last-context ((contexts xlib-contexts))
  (first (last (slot-value contexts 'contexts))))

(defmethod print-object ((object xlib-context) stream)
  (format stream "#<XLIB-CONTEXT <color ~a> <text ~s> <x ~a> <y ~a>>"
          (text-color object)
          (text object)
          (x object)
          (y object)))

(defmethod text-width ((window window) text)
  (xlib:text-width (font window) text))
