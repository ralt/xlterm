(in-package #:xlterm)

(flag "-lutil")

(include "pty.h")

(defwrapper ("forkpty" forkpty) pid-t
  (amaster (:pointer :int) :type "int*")
  (name :string :type "char*")
  (term :pointer :type "const struct termios*")
  (winp :pointer :type "const struct winsize*"))
