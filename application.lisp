(in-package #:xlterm)

(cffi:defcvar "errno" :int)

(defvar *application*)

(defclass application ()
  ((title :accessor title :initform "xlterm")
   (background :accessor background :initform "black")
   (foreground :accessor foreground :initform "white")
   (font-family :accessor font-family :initform "fixed")
   (window :accessor window)
   (rows-count :accessor rows-count)
   (master-fd :accessor master-fd)
   (child-pid :accessor child-pid)
   (characters :accessor characters :initform nil)
   (print-variable :accessor print-variable)
   (print-lock :accessor print-lock)
   (read-thread :accessor read-thread)
   (print-thread :accessor print-thread)))

(defun start (application)
  (draw (setf (window application) (make-instance 'window
                                                  :title (title application)
                                                  :background-color (background application)
                                                  :foreground-color (foreground application)
                                                  :font-family (font-family application)
                                                  :event-mask '(:exposure :key-press))))

  (multiple-value-bind (pid master-fd)
      (start-shell)
    (setf (master-fd application) master-fd)
    (setf (child-pid application) pid))

  (setf (print-variable application) (bt:make-condition-variable))
  (setf (print-lock application) (bt:make-lock))

  (setf (read-thread application) (bt:make-thread #'read-loop
                                                  :name "read"
                                                  :initial-bindings `((*application* . ,application))))
  (setf (print-thread application) (bt:make-thread #'print-loop
                                                   :name "print"
                                                   :initial-bindings `((*application* . ,application))))

  (setf (rows-count application) (/ (height (window application)) (font-ascent (window application))))

  (handle-events application))

(defun stop (application)
  (sb-unix:unix-kill (child-pid application) 15)
  (bt:destroy-thread (print-thread application))
  (bt:destroy-thread (read-thread application))
  (destroy (window application)))

(defun handle-events (application)
  (let ((window (window application)))
    (on-event
        window
      (:exposure
       ()
       (progn
         (bt:condition-notify (print-variable application))
         nil))
      (:key-press
       (code)
       (let* ((char (code->char window code))
              (buffer (make-array 1
                                  :initial-element char
                                  :element-type 'character
                                  :fill-pointer nil
                                  :adjustable nil
                                  :displaced-to nil)))
         (unless (eq (type-of char) 'keyword)
           (sb-unix:unix-write (master-fd application) buffer 0 1)
           nil))))))

(defun read-loop ()
  (loop
     (cffi:with-foreign-object (buf :char 16384)
       (let ((read-bytes (sb-unix:unix-read (master-fd *application*) buf 16384)))
         (if (or (eq read-bytes nil) (< read-bytes 0))
             (format t "errno: ~a~%" *errno*)
             (progn
               (bt:with-lock-held ((print-lock *application*))
                 (add-characters *application*
                                 (loop
                                    for i below read-bytes
                                    collect (cffi:mem-ref buf :char i))))
               (bt:condition-notify (print-variable *application*))))))))

(defun print-loop ()
  (bt:with-lock-held ((print-lock *application*))
    (loop
       (let ((window (window *application*)))
         (bt:condition-wait (print-variable *application*) (print-lock *application*))
         (clear window)
         (let ((parser (make-instance 'parser
                                      :input (characters *application*)
                                      :default-foreground (foreground *application*)
                                      :window window)))
           (draw-text window (contexts parser)))
         (flush window)))))

(defun add-characters (application new-chars)
  (loop for char in new-chars
     do (appendf (characters application) (list char))))
